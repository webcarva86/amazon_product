const path = require('path');

const rootPath = path.resolve(__dirname, './src/');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isDevelopment = process.env.NODE_ENV !== 'production';

// the path(s) that should be cleaned
const pathsToClean = [
  'dist',
];

const cleanOptions = {
  verbose: true,
  dry: false
};

const baseFiles = ['@babel/polyfill'];

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './default.html',
});

const compressionPlugin = new CompressionPlugin({});
const copyWebpackPlugin = new CopyWebpackPlugin([
  { from: 'src/assets', to: 'assets' },
]);

const cleanWebpackPlugin = new CleanWebpackPlugin(pathsToClean, cleanOptions);

const envPlugins = [
  htmlPlugin,
  compressionPlugin,
  copyWebpackPlugin,
];

if (!isDevelopment) envPlugins.push(cleanWebpackPlugin);

module.exports = {
  devtool: isDevelopment ? 'cheap-module-eval-source-map' : 'source-map',
  entry: {
    vendor: [
      'prop-types',
      'react',
      'react-dom',
      'react-jss',
      'react-redux',
      'react-router-dom',
      'redux',
      'redux-thunk',
    ],
    app: baseFiles.concat(['./src/client.jsx']),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].chunkhash.bundle.js',
    chunkFilename: '[name].chunkhash.bundle.js',
    publicPath: '/',
  },
  resolve: {
    alias: {
      components: path.resolve(rootPath, 'components'),
      containers: path.resolve(rootPath, 'containers'),
      reducers: path.resolve(rootPath, 'reducers'),
      actions: path.resolve(rootPath, 'actions'),
      theme: path.resolve(rootPath, 'theme'),
      assets: path.resolve(rootPath, 'assets'),
    },
    extensions: ['.js', '.jsx'],
  },
  devServer: {
    historyApiFallback: {
      index: 'dist/default.html'
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader?url=false' },
        ],
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 8000, // Convert images < 8kb to base64 strings
            name: 'assets/[hash]-[name].[ext]',
          },
        }],
      },
    ],
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true,
        },
      },
      minChunks: Infinity,
    },
  },
  plugins: envPlugins,
};
