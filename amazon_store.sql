-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2018 at 05:09 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amazon_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `json` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

-- INSERT INTO `products` (`id`, `p_id`, `json`) VALUES
-- (8, 'B002QYW8LW', '{\n    \"domTitle\": \"Baby Banana Infant Training Toothbrush and Teether, Yellow\",\n    \"domDescription\": \"Color:Infant  Product description 100% Food Grade silicone teething toothbrush for kids 3-12 months of age. Specifically designed with \\u201ca-peel-ing\\u201d handles that are easy for baby to hold, while preventing choking. Bendable soft silicone reduces risk of injury, providing the safest learning experience possible. Dishwasher and Freezer Friendly. BPA, Phthalate, Latex, and Toxin Free. Helps to soothe sore teething gums with durable massaging bristles. Inspired by a Registered Dental Hygienist. Brings teething and brushing together, helping babies to develop healthy oral care habits from the earliest age possible, literally taking dental care \\u201cinto their own hands.\\u201d Helps develop hand\\/eye\\/mouth coordination Recommended by Pediatric Dentists 100% Food grade silicone BPA, Latex, Phthalate, toxin free Durable and long lasting, no need to replace Dishwasher and freezer friendly Safer than traditional toothbrushes Developed by a Dental Hygienist Promotes healthy habits from the beginning Massaging Bristles for teething relief Helps develop hand\\/eye\\/mouth coordination Removes food and milk particles gently Engineered with stimulating textures toxin free Durable and long lasting, no need to replace Dishwasher and freezer friendly Safer than traditional toothbrushes Developed by a Dental Hygienist Promotes healthy habits from the beginning Massaging Bristles for teething relief Helps develop hand\\/eye\\/mouth coordination Removes food and milk particles gently Engineered with stimulating textures  Brand Story By   See all Product description\",\n    \"domPrice\": \"$7.61\",\n    \"domImage\": \"https:\\/\\/images-na.ssl-images-amazon.com\\/images\\/I\\/71vLVbAlWXL._SL1500_.jpg\",\n    \"domRank\": \"Best Sellers Rank#7 in Baby (See top 100)  #1 in\\u00a0Baby &gt; Baby Care &gt; Health   #2 in\\u00a0Baby &gt; Baby Care &gt; Pacifiers, Teethers &amp; Teething Relief &gt; Teethers\",\n    \"domDimensions\": \"4.3 x 0.4 x 7.9 inches\",\n    \"domCategory\": [\n        \"   Baby Products    \",\n        \"    Baby Care    \",\n        \"    Pacifiers, Teethers &amp; Teething Relief    \",\n        \"    Teethers   \"\n    ]\n}'),
-- (9, 'B07D36KNV2', '{\n    \"domTitle\": \"SOMEK Baby Teething Toys Toothbrush and Teethers for Infant Training and Teething Relief with Bristles,100% FDA&amp;BPA-Free Silicone, Cactus Shape and Hands Free Design(Green)\",\n    \"domDescription\": \"Color:Green  SOMEK Silicone Cactus Teether is a good choice for you and your little one.You deserve better!\\u2605\\u2605\\u2605It\\u00a0is a\\u00a0100% Food Grade silicone teething toothbrush for kids 3-12 months of age. BPA, Phthalate, Latex and Toxin Free.\\u2605\\u2605\\u2605Its super soft silicone materiel\\u00a0and unique textured design provides comfort to baby\\u2019s delicate gums. It\\u00a0also provides stimulus to the lips and tongue and assists in the eruption of new teeth. And\\u00a0 will not attache much fire or dirt, which is easy to keep clean.\\u2605\\u2605\\u2605The cactus shape, the\\u00a0bright colors and interesting textures stimulate baby\'s senses of sight and touch while alleviating the pain of teething, the small size is easy for baby to hold, while preventing choking.Your little baby will enjoy and love it!\",\n    \"domPrice\": \"$7.98\",\n    \"domImage\": \"https:\\/\\/images-na.ssl-images-amazon.com\\/images\\/I\\/51crt0plhTL._SL1000_.jpg\",\n    \"domRank\": \"N\\/A\",\n    \"domDimensions\": \"N\\/A\",\n    \"domCategory\": [\n        \"   Baby Products    \",\n        \"    Baby Care    \",\n        \"    Pacifiers, Teethers &amp; Teething Relief    \",\n        \"    Teethers   \"\n    ]\n}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
