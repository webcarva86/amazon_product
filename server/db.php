<?php 
  function DBOpenCon() {
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $db = "amazon_store";
    
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);
    
    return $conn;
  }
  
  function DBCloseCon($conn) {
    $conn -> close();
  }

  function DBGetProduct($productId) {
    $conn = DBOpenCon();
    $sql = "SELECT json FROM products WHERE p_id='".addslashes($productId)."'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $json = stripslashes($row['json']);
      }
      DBCloseCon($conn);
      return $json;
    } else {
      DBCloseCon($conn);
      return false;
    }
  }

  function DBStoreProduct($productId, $productData) {
    $conn = DBOpenCon();
    $sql = "INSERT INTO products VALUES ('', '".addslashes($productId)."', '".addslashes($productData)."')";
    $result = $conn->query($sql);

    if ($result === TRUE) {
      DBCloseCon($conn);
      return $result;
    } else {
      error_log("Error: " . $sql . "<br>" . $conn->error, 0);
    }
    DBCloseCon($conn);
  }
 ?>