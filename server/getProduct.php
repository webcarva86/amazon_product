
<?php 
  error_reporting(0);
  include('db.php');

  class SpecificObject {
    function SpecificObject() {
      $this->domTitle = "N/A";
      $this->domDescription = "N/A";
      $this->domPrice = "N/A";
      $this->domImage = "N/A";
      $this->domRank = "N/A";
      $this->domDimensions = "N/A";
      $this->domCategory = "N/A";
    }
  }

  /* gets the data from a URL */
  function get_data($productId) {
    if (DBGetProduct($productId) !== false) {
      return DBGetProduct($productId);
    } else {
      $url = "https://www.amazon.com/dp/$productId";
      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
      $data = curl_exec($ch);
      curl_close($ch);
      $productData = html_to_obj($data);
      if ($productData !== false) {
        $jsonProductData = json_encode($productData, JSON_PRETTY_PRINT);
        DBStoreProduct($productId, $jsonProductData);
        return $jsonProductData;
      } else {
        return '{}';
      }
    }
  }

  function html_to_obj($html) {
    $doc = new DOMDocument();
    $obj = new SpecificObject();
    $doc->loadHTML($html);
    $title = $doc->getElementById('productTitle');
    if ($title) {
      $obj->domTitle = trim(strip_tags( preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "",  $title->C14N()))));
    }
    $description = $doc->getElementById('productDescription');
    if ($description) {
      $obj->domDescription = trim(strip_tags( preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "",  $description->C14N()))));
    }      
    $price = $doc->getElementById('priceblock_ourprice');
    if ($price) {
      $obj->domPrice = trim(strip_tags( preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "",  $price->C14N()))));
    }
    $rank = $doc->getElementById('SalesRank');
    if ($rank) {
      $obj->domRank = trim(strip_tags( preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "",  $rank->C14N()))));
    }

    $finder = new DomXPath($doc);
    $nodes = $finder->query("//*[contains(@class, 'size-weight')][2]/td[@class='value']/text()");
    $tmp_dom = new DOMDocument();
    foreach ($nodes as $node) 
    {
      $tmp_dom->appendChild($tmp_dom->importNode($node,true));
    }
    $dimensions = trim($tmp_dom->saveHTML()); 
    if ($dimensions) {
      $obj->domDimensions = $dimensions;
    }

    $finder = new DomXPath($doc);
    if ($finder->query("//div[contains(@id, 'imgTagWrapperId')]/img")->item(0)) {
      $obj->domImage = $finder->query("//div[contains(@id, 'imgTagWrapperId')]/img")->item(0)->getAttribute('data-old-hires');
    }
    
    $category = $doc->getElementById('wayfinding-breadcrumbs_feature_div');
    if ($category) {
      $obj->domCategory = explode('›', strip_tags( preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "",  $category->C14N()))));
    }
    return $obj;
  }

  if (isset($_GET)) {
    $productId = addslashes($_GET['productId']);
    echo get_data($productId);
  }
?>