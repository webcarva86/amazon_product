# Amazon Product Store

## Setup Server
* install xampp (for php, apache, mysql) - https://www.apachefriends.org/index.html
* update host, user, password for mysql in /server/db.php  DBOpenCon()
* import amazon_store.sql

## Setup UI
* npm i

## Run App
* npm run start:dev