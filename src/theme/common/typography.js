import colors from 'theme/common/colors';
import { md } from 'theme/mediaRules';
import {
  fontFamily,
  fontFamilyBig,
  fontWeightRegular,
  fontWeightMedium,
  fontWeightSemiBold,
  fontWeightBold,
} from 'theme/common/fontFamily';

const typography = {
  fontFamily,
  fontSize: 14,
  fontWeightRegular,
  fontWeightMedium,
  fontWeightSemiBold,
  fontWeightBold,
  display4: {
    fontSize: '20px',
    lineHeight: '26px',
    marginTop: '18px',
    marginBottom: '18px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[100],
    [md]: {
      fontSize: '30px',
      lineHeight: '40px',
      marginBottom: '24px',
    },
  },
  display3: {
    fontSize: '18px',
    lineHeight: '24px',
    marginTop: '18px',
    marginBottom: '18px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[100],
    [md]: {
      fontSize: '25px',
      lineHeight: '32px',
    },
  },
  display2: {
    fontSize: '16px',
    lineHeight: '22px',
    marginTop: '16px',
    marginBottom: '16px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[100],
    [md]: {
      fontSize: '21px',
      lineHeight: '28px',
      marginBottom: '16px',
      letterSpacing: 0,
    },
  },
  display1: {
    fontSize: '15px',
    lineHeight: '21px',
    marginTop: '8px',
    marginBottom: '8px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[100],
    [md]: {
      fontSize: '17px',
      lineHeight: '23px',
      marginBottom: '12px',
    },
  },
  title: {
    fontSize: '15px',
    lineHeight: '24px',
    marginTop: '12px',
    marginBottom: '12px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[85],
  },
  // see below for body1 styles
  body2: {
    fontSize: '13px',
    lineHeight: '18px',
    marginBottom: '12px',
    letterSpacing: 0,
    fontWeight: fontWeightRegular,
    fontFamily,
    color: colors.blueGreyScale[85],
    marginTop: 0,
    [md]: {
      fontSize: '15px',
      lineHeight: '24px',
      letterSpacing: 0,
    },
  },
  button: {
    fontSize: '13px',
    lineHeight: '17px',
    letterSpacing: 0,
    textTransform: 'uppercase',
    fontWeight: fontWeightBold,
    fontFamily,
    color: colors.blueGreyScale[85],
    [md]: {
      lineHeight: '18px',
    },
  },
  subheading: {
    fontSize: '12px',
    lineHeight: '18px',
    letterSpacing: 0,
    fontWeight: fontWeightBold,
    fontFamily: fontFamilyBig,
    color: colors.blueGreyScale[100],
    [md]: {
      fontSize: '13px',
      lineHeight: '18px',
      letterSpacing: 0,
    },
  },
  caption: {
    fontSize: '9px',
    lineHeight: '14px',
    marginBottom: 0,
    letterSpacing: 0,
    fontWeight: fontWeightRegular,
    fontFamily,
    color: colors.blueGreyScale[85],
    [md]: {
      fontSize: '11px',
      lineHeight: '15px',
      letterSpacing: 0,
    },
  },
};

// Extension from original values
typography.body1 = {
  ...typography.body2,
  color: colors.blueGreyScale[100],
  textTransform: 'none',
};

export default typography;
