import { fontWeightMedium } from 'theme/common/fontFamily';
import typography from 'theme/common/typography';
import colors from 'theme/common/colors';
import breakpoints from 'theme/common/breakpoints';

// media query helpers
const maxSm = `@media (max-width: ${breakpoints.values.sm}px)`;

const bodyText = {
  ...typography.body1,
  textTransform: 'none',
};

const tooltipStyle = {
  backgroundColor: colors.blueGreyScale[100],
  fontSize: '11px',
  lineHeight: '13px',
  color: colors.common.white,
  padding: '5px',
};

const tooltipArrow = {
  content: '""',
  position: 'absolute',
  borderWidth: '5px',
  borderStyle: 'solid',
  zIndex: '1500',
};

const overrides = {
  MuiAppBar: {
    root: {
      backgroundColor: 'transparent',
    },
  },
  MuiBackdrop: {
    root: {
      backgroundColor: 'rgba(51, 62, 72, 0.9)',
    },
  },
  MuiButton: {
    root: {
      fontWeight: fontWeightMedium,
    },
  },
  MuiDialogContentText: {
    root: bodyText,
  },
  MuiFormControlLabel: {
    label: {
      fontSize: '14px',
      marginBottom: 0,
      marginLeft: '-4px',
    },
  },
  MuiFormHelperText: {
    root: {
      marginTop: 0,
    },
  },
  MuiInput: {
    root: {
      color: colors.secondary.main,
    },
    focused: {
      '& svg': {
        color: `${colors.primary.dark}`,
      },
    },
  },
};

export default overrides;
