export default {
  primary: {
    main: '#0D86D5',
    light: '#1A97E9',
    dark: '#0D74B8',
    contrastText: '#fff',
  },
  secondary: {
    main: '#5C6975',
    light: '#7A8B9B',
    dark: '#4B555E',
    contrastText: '#1D1E21',
  },
  common: {
    white: '#FFF',
  },
  red: {
    100: '#D64428',
  },
  orange: {
    100: '#FFA500',
  },
  yellow: {
    100: '#FDBD1D',
  },
  green: {
    100: '#449A4F',
  },
  purple: {
    100: '#800080',
  },
  blue: {
    100: '#247FBB',
  },
  greyScale: {
    100: '#3A3D3F',
  },
  blueGreyScale: {
    100: '#333E48',
  },
};
