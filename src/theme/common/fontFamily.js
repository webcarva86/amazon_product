export const fontFamily = "'Roboto', sans-serif";
export const fontWeightRegular = 400;
export const fontWeightMedium = 500;
export const fontWeightSemiBold = 600;
export const fontWeightBold = 700;

export const fontFamilyBig = "'Podkova', sans-serif";

export default {
  fontFamilyBig,
  fontFamily,
  fontWeightRegular,
  fontWeightMedium,
  fontWeightSemiBold,
  fontWeightBold,
};
