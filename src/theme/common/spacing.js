export default {
  unit: 8,
  textInputMargins: {
    marginBottom: '8px',
    marginRight: '10px',
  },
};
