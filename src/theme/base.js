import typography from 'theme/common/typography';
import colors from 'theme/common/colors';
import overrides from 'theme/common/overrides';
import spacing from 'theme/common/spacing';
import breakpoints from 'theme/common/breakpoints';

export default {
  palette: colors,
  breakpoints,
  overrides,
  spacing,
  typography,
};
