import baseTheme from 'theme/base';
import { fontFamily } from 'theme/common/fontFamily';

export default {
  '@global': {
    'html, body, input, textarea, button': {
      letterSpacing: 0,
      '-webkit-font-smoothing': 'antialiased',
      '-moz-osx-font-smoothing': 'grayscale',
      '-webkit-text-size-adjust': '100%',
      '-moz-osx-text-size-adjust': '100%',
    },
    body: {
      fontFamily,
    },
    a: {
      color: baseTheme.palette.primary.main,
      textDecoration: 'none',
      '&:link, &:active, &:visited': {
        color: baseTheme.palette.primary.main,
        textDecoration: 'none',
      },
      '&:hover': {
        color: baseTheme.palette.primary.light,
        textDecoration: 'none',
      },
    },
  },
};
