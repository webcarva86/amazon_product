import breakpoints from 'theme/common/breakpoints';

/* **************** USE CASE **************** */
/*
  import mediaRules from 'client/theme/mediaRules';
  ...
  className: {
    backgroundColor: theme.palette.blueGreyscale.light,
    [`${mediaRules('md')}`]: {
      display: 'flex',
    },
  },
*/
/* ****************************************** */

const mediaRules = (display) => {
  let mediaRule = '';
  switch (display) {
    case 'xs':
      mediaRule = [`@media (min-width:${breakpoints.values.xs}px)`];
      break;
    case 'sm':
      mediaRule = [`@media (min-width:${breakpoints.values.sm}px)`];
      break;
    case 'md':
      mediaRule = [`@media (min-width:${breakpoints.values.md}px)`];
      break;
    case 'lg':
      mediaRule = [`@media (min-width:${breakpoints.values.lg}px)`];
      break;
    case 'xl':
      mediaRule = [`@media (min-width:${breakpoints.values.xl}px)`];
      break;
    default:
      break;
  }
  return mediaRule;
};

export const xs = mediaRules('xs');
export const sm = mediaRules('sm');
export const md = mediaRules('md');
export const lg = mediaRules('md');
export const xl = mediaRules('md');
