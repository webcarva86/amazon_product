import {
  createStore, applyMiddleware, compose, combineReducers
} from 'redux';
import thunk from 'redux-thunk';
import persistState from 'redux-localstorage';
import reducers from '../reducers/root.js';

const enhancer = compose(
  persistState(['products']),
  applyMiddleware(thunk),
  process.env.NODE_ENV !== 'production'
  && window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f,
);

const initalState = {};

export const store = createStore(
  combineReducers({
    ...reducers,
  }),
  initalState,
  enhancer
);
