import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchProduct } from 'actions/root';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
import ShopIcon from '@material-ui/icons/ShopTwoTone';

import Loading from 'components/Loading';
import Modal from 'components/Modal';
import Products from 'components/Products';
import SearchForm from 'components/SearchForm';

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
  },
  alignRight: {
    textAlign: 'right',
  },
  alignCenter: {
    textAlign: 'center',
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    textTransform: 'uppercase',
    paddingTop: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,
    transition: theme.transitions.create('width'),
    [theme.breakpoints.up('sm')]: {
      '&:focus': {
        width: '100%',
      },
    },
  },
  card: {
    maxWidth: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '40px',
    animate: ''
  },
  textField: {
    marginRight: theme.spacing.unit,
  },

  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  item: {
    flex: '1 1 50%',
    minWidth: '300px',
  },
  image: {
    maxWidth: '500px',
    '& img': {
      maxWidth: '100%',
    }
  }
});

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.handleGetProduct = this.handleGetProduct.bind(this);
  }

  handleGetProduct(params) {
    return this.props.fetchProduct(params);
  }

  render() {
    const {
      classes, products, loading, error
    } = this.props;

    return (
      <div className={classNames(classes.root)}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classNames(classes.menuButton)} color="inherit" aria-label="Open drawer">
              <ShopIcon />
            </IconButton>
            <Typography variant="title" color="inherit" noWrap>
              Amazing Amazon Product Search UI
            </Typography>
            <div className={classNames(classes.grow)} />
          </Toolbar>
        </AppBar>
        <div>
          <SearchForm fetchProduct={this.handleGetProduct} />
        </div>
        {error ? (
          <Modal title="A problem has occured... " content={error} />
        ) : null}
        <div>
          {loading && <Loading />}
          {(!loading && Object.keys(products).length !== 0) && (
            <Products products={products} />
          )}
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  fetchProduct: PropTypes.func.isRequired,
  products: PropTypes.shape({}).isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

Home.defaultProps = {
  error: '',
};

const mapStateToProps = state => ({
  products: state.product.items,
  loading: state.product.loading,
  error: state.product.error,
});

const mapDispatchToProps = dispatch => bindActionCreators({ fetchProduct }, dispatch);

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Home));
