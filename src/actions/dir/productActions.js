export const FETCH_PRODUCT_BEGIN = 'FETCH_PRODUCT_BEGIN';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const fetchProductBegin = () => ({
  type: FETCH_PRODUCT_BEGIN
});

export const fetchProductSuccess = products => ({
  type: FETCH_PRODUCT_SUCCESS,
  payload: { products }
});

export const fetchProductError = error => ({
  type: FETCH_PRODUCT_FAILURE,
  payload: {
    errorInternal: error,
    error: 'Oh No: Looks like there has been an issue accessing that product. Apologies for the inconvenience',
  },
});

export function fetchProduct(params) {
  const headers = new Headers();
  headers.append('Access-Control-Allow-Origin', '*');
  headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Access-Control-Allow-Headers, Access-Control-Request-Method, Access-Control-Request-Headers');
  return (dispatch) => {
    dispatch(fetchProductBegin());
    return fetch(`http://localhost:80/amazon_product/server/getProduct.php?productId=${params}`, {
      mode: 'cors',
      headers
    })
      .then((response) => {
        if (!response.ok || response.status >= 400) {
          return response
            .text()
            .then(res => (res ? JSON.parse(res) : {}))
            .then(error => Promise.reject(new Error(error.statusText)));
        }
        return response;
      })
      .then(res => res.json())
      .then((json) => {
        dispatch(fetchProductSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchProductError(error)));
  };
}
