import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grow from '@material-ui/core/Grow';

const styles = theme => ({
  grow: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
  },
  alignRight: {
    textAlign: 'right',
  },
  alignCenter: {
    textAlign: 'center',
  },
  card: {
    maxWidth: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '40px',
    animate: ''
  },
  textField: {
    marginRight: theme.spacing.unit,
  },

  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  item: {
    flex: '1 1 50%',
    minWidth: '300px',
  },
  image: {
    maxWidth: '500px',
    '& img': {
      maxWidth: '100%',
    }
  }
});

function Products(props) {
  const { classes, products } = props;
  return (
    <React.Fragment>
      <Grow in>
        <Card className={classes.card}>
          <CardContent>
            <div className={classNames(classes.grow, classes.alignCenter)}>
              <Typography color="primary" variant="display3" gutterBottom>
                {products.domTitle}
              </Typography>
            </div>
            <div className={classNames(classes.grow, classes.container)}>
              <div className={classNames(classes.item, classes.image)}>
                <img src={products.domImage} alt={products.domTitle} />
              </div>
              <div className={classNames(classes.item)}>
                <Typography variant="title" gutterBottom>Description</Typography>
                <Typography variant="body1" gutterBottom>
                  {products.domDescription}
                </Typography>
              </div>
              <div className={classNames(classes.item)}>
                <Typography variant="title" gutterBottom>Price</Typography>
                <Typography variant="body1" gutterBottom>
                  {products.domPrice}
                </Typography>
              </div>
              <div className={classNames(classes.item)}>
                <Typography variant="title" gutterBottom>Rank</Typography>
                <Typography variant="body1" gutterBottom>
                  {products.domRank}
                </Typography>
              </div>
              <div className={classNames(classes.item)}>
                <Typography variant="title" gutterBottom>Dimensions</Typography>
                <Typography variant="body1" gutterBottom>
                  {products.domDimensions}
                </Typography>
              </div>
              <div className={classNames(classes.item)}>
                <Typography variant="title" gutterBottom>Categories</Typography>
                <ul>
                  {products.domCategory && products.domCategory.map(item => (
                    <li key={item}><Typography variant="body1" gutterBottom>{item}</Typography></li>
                  ))}
                </ul>
              </div>
            </div>
          </CardContent>
        </Card>
      </Grow>
    </React.Fragment>
  );
}

Products.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  products: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(Products);
