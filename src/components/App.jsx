import React from 'react';
import PageHome from 'containers/Home';

import {
  withStyles,
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';
import globalTheme from 'theme/globals';
import mainTheme from 'theme/base';

const theme = createMuiTheme(mainTheme);

const styles = () => globalTheme;

const App = () => (
  <MuiThemeProvider theme={theme}>
    <PageHome />
  </MuiThemeProvider>
);

export default withStyles(styles)(App);
