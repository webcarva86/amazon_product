import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grow from '@material-ui/core/Grow';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';


const styles = theme => ({
  grow: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
  },
  alignRight: {
    textAlign: 'right',
  },
  alignCenter: {
    textAlign: 'center',
  },
  card: {
    maxWidth: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '40px',
    animate: ''
  },
  textField: {
    marginRight: theme.spacing.unit,
  },

  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  item: {
    flex: '1 1 50%',
    minWidth: '300px',
  },
  image: {
    maxWidth: '500px',
    '& img': {
      maxWidth: '100%',
    }
  }
});

class SearchForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      errorText: '',
      searchValue: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleGetProduct = this.handleGetProduct.bind(this);
  }

  handleGetProduct() {
    this.props.fetchProduct(this.state.searchValue);
  }

  handleChange(event) {
    const { value } = event.target;
    const input = value;
    let error = false;
    let errorText = '';
    if (input.length !== 10) {
      errorText = 'Product key should be 10 characters';
      error = true;
    } else if (!input.match(/^[a-z0-9]+$/i)) {
      errorText = 'Product key should alphanumeric only (Letters/Numbers)';
      error = true;
    } else {
      errorText = '';
      error = false;
    }

    this.setState({
      searchValue: value,
      error,
      errorText,
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Grow in>
          <Card className={classes.card}>
            <CardContent>
              <div className={classNames(classes.grow, classes.alignCenter)}>
                <Typography color="textSecondary" variant="display3" gutterBottom>
                  Enter your amzon product key below
                </Typography>
              </div>
              <div className={classNames(classes.search)}>
                <div className={classNames(classes.searchIcon)} />
                <FormControl fullWidth>
                  <TextField
                    error={this.state.error}
                    helperText={this.state.errorText}
                    type="search"
                    className={classes.textField}
                    variant="outlined"
                    placeholder="Search…"
                    onKeyPress={(ev) => {
                      if (ev.key === 'Enter') {
                        ev.preventDefault();
                        this.handleGetProduct();
                      }
                    }}
                    onChange={this.handleChange}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>,
                      classes: { input: classes.inputInput },
                      inputProps: {
                        maxLength: 10,
                      }
                    }}
                    required
                  />
                </FormControl>
              </div>
            </CardContent>
            <CardActions>
              <div className={classNames(classes.grow, classes.alignRight)}>
                <Button
                  variant="raised"
                  size="medium"
                  color="primary"
                  onClick={this.handleGetProduct}
                  disabled={this.state.searchValue.length === 0 || this.state.error}
                >
                  <SearchIcon /> Search Product
                </Button>
              </div>
            </CardActions>
          </Card>
        </Grow>
      </React.Fragment>
    );
  }
}

SearchForm.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  fetchProduct: PropTypes.func.isRequired,
};

export default withStyles(styles)(SearchForm);
