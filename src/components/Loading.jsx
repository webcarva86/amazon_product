import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import Typography from '@material-ui/core/Typography';

const styles = {
  loadingLabel: {
    textAlign: 'center',
    color: '#fff',
  },
  loader: {
    textAlign: 'center',
    '& > span': {
      display: 'inline-block',
      verticalAlign: 'middle',
      width: '10px',
      height: '10px',
      margin: '50px auto',
      background: '#fff',
      borderRadius: '50px',
      animation: 'loader 0.9s infinite alternate',
      '&:nth-of-type(2)': {
        animationDelay: '0.3s',
      },
      '&:nth-of-type(3)': {
        animationDelay: '0.6s',
      }
    },
  },
  '@keyframes loader': {
    '0%': {
      width: '10px',
      height: '10px',
      opacity: '0.9',
      transform: 'translateY(0)',
    },
    '100%': {
      width: '24px',
      height: '24px',
      opacity: '0.1',
      transform: 'translateY(-21px)',
    }
  },
};

function Loading(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <div className={classNames(classes.loadingLabel)}>
        <Typography variant="display2" className={classNames(classes.loadingLabel)}>Loading</Typography>
      </div>
      <div className={classNames(classes.loader)}>
        <span />
        <span />
        <span />
      </div>
    </React.Fragment>
  );
}

Loading.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(Loading);
